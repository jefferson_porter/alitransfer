'''
Created on Jan 26, 2015

Module handling with the communication with the two collections in the
ALICE Mongo database dealing with transfers (transferqueue and tansferjobs)

@author: markus
'''

import pymongo
from Datatransfer.Struct.FileInfo import FileInfo
from Datatransfer.Struct.TransferJob import TransferJob

class TransferDBConnector(object):
    """
    Connection to the transfer DB for the transfer queue and the transfer jobs
    """

    def __init__(self, databaseConnection):
        '''
        Constructor
        '''
        self.__mongoclient  = pymongo.MongoClient(databaseConnection)
        self.__database     = self.__mongoclient["alice"]

    def __GetTransferQueueCollection(self):
        return self.__database["transferqueue"]

    def __GetTransferJobCollection(self):
        return self.__database["transferjobs"]

    def GetNumberOfQueueEntries(self):
        '''
        Get the number of unfinished entries in the queue
        '''
        transferqueue = self.__GetTransferQueueCollection()
        entriesInQueue = transferqueue.find({"Status":"PEND"})
        if not entriesInQueue:
            return 0
        return entriesInQueue.count()

    def HasJobs(self):
        '''
        Check whether there are unfinished jobs in the queue
        '''
        return self.GetNumberOfQueueEntries() != 0

    def GetNextEntryInQueue(self):
        """
        Get next entry from the transfer queue
        """
        transferqueue = self.__GetTransferQueueCollection()
        queueentry = FileInfo()
        myentry = transferqueue.find_one({"Status":"PEND"})
        if not myentry:
            return None
        # Mark as running, before this remove the OID from the entry
        myentry["Status"] = "RUN";
        self.UpdateTransferQueueEntryValues(myentry["_id"], {"Status":"RUN"})
        queueentry.Initialise(myentry)

        # Check whether a matching transfer job exists, and set the status from
        if len(queueentry.GetTransferJob()):
            self.UpdateTransferJobStatus(queueentry.GetTransferJob(), "RUN")
            
        return queueentry
    
    def UpdateTransferQueueEntryValues(self, entryID, fields):
        """
        Update fields for a given entry in the transfer queue. A valid MongoDB entryID is 
        expected. Fiels is expected to be a dictionary with fields for the entry to modify
        """
        queuedb = self.__GetTransferQueueCollection()
        for k,v in fields.iteritems():
            queuedb.update({"_id": entryID},{"$set":{k:v}}, upsert = False, multi = False)

    def InsertTransferQueueEntry(self, fileinf):
        """
        Insert new file to the transfer queue
        """
        transferqueue = self.__GetTransferQueueCollection()
        transferqueue.save(fileinf.Serialize())
        
    def RemoveEntryFromQueue(self, entryID):
        """
        Remove entry from the transfer queue. A valid MongoDB entry ID is required
        """
        transferqueue = self.__GetTransferQueueCollection()
        if transferqueue.find_one({"_id": entryID}):
            transferqueue.remove({"_id": entryID})
            
    def RemoveEntryByKeyFromQueue(self, key, value):
        """
        Remove entry, identified by a key/value pair from the queue
        """
        transferqueue = self.__GetTransferQueueCollection()
        transferqueue.remove({key, value})
            
    def RemoveDoneEntriesFromQueue(self):
        """
        Remove all entries which are in status DONE from the queue
        """
        transferqueue = self.__GetTransferQueueCollection()
        transferqueue.remove({"Status":"DONE"})

    def InsertTransferJob(self, transferjob):
        """
        Insert transfer job
        put also single files to the queue
        """
        jobdb = self.__GetTransferJobCollection()
        jobdb.save(transferjob.Serialize())
        # Put files to the queue
        for entry in transferjob.GetListOfFiles():
            self.InsertTransferQueueEntry(entry)
            
    def UpdateFileTransferStatus(self, fileinf, newstatus):
        """
        Update the transfer status of a given file
        """
        transferqueue = self.__GetTransferQueueCollection()
        entryID = fileinf.GetID()
        if not entryID:
            # identify entry via source location
            myentry = transferqueue.find_one({"SourceLocation"})
            if myentry:
                entryID = myentry["_id"]
        if entryID:
            self.UpdateTransferQueueEntryValues(entryID, {"Status":newstatus})

    def UpdateFileStatusInDatabase(self, fileinf):
        """
        Update status of the file in the transfer queue database
        Also updates the status of the file in the corresponding
        transfer job. If each file is either in DONE or error, the
        transfer job status is changed to DONE
        """
        transferqueue =  self.__GetTransferQueueCollection()
        myentry = transferqueue.find_one({"_id":fileinf.GetID()})
        myentry["Status"] = fileinf.GetStatus()
        modentry = {}
        for k,v in myentry.iteritems():
            if k == "_id":
                continue
            modentry[k] = v
        transferqueue.update({"_id":fileinf.GetID()}, {"$set":modentry}, upsert = False)

        # Update file status in the full transfer job
        if len(fileinf.GetTransferJob()):
            job = self.GetTransferJob(fileinf.GetTransferJob())
            job.SetFileStatus(fileinf.GetSourceLocation(), fileinf.GetStatus())
            if job.GetPercentageFinal() == 1.:
                job.SetStatus("DONE")
            self.UpdateTransferJob(job)


    def UpdateTransferJob(self, transferjob):
        """
        Update a transfer job with new information
        """
        jobdb = self.__GetTransferJobCollection()
        jobdb.update({"_id":transferjob.GetID()}, {"$set":transferjob.Serialize()}, upsert = False)
        
    def UpdateTransferJobValues(self, entryID, entries):
        """
        Updates only single values of a transferjob
        Must come with a valid MongoDB ID (entryID)
        entries is expected to be a dictionary with a key/value pair of fields to modify
        """
        jobdb = self.__GetTransferJobCollection()
        for mykey,myvalue in entries.iteritems():
            jobdb.update({"_id": entryID}, {"$set":{mykey:myvalue}}, upsert = False, multi = False)
            
    def UpdateTransferJobStatus(self, transferjobname, newstatus):
        """
        Update status of the transfer job
        """
        myjob = self.GetTransferJob(transferjobname)
        if myjob.GetStatus() != newstatus:
            self.UpdateTransferJobValues(myjob.GetID(), {"Status":newstatus})

    def GetTransferJob(self, jobname):
        """
        Access to information for full transfer job
        """
        jobdb = self.__GetTransferJobCollection()
        jobentry = jobdb.find_one({"Name":jobname})
        if jobentry:
            result = TransferJob(jobname)
            result.Initialize(jobentry)
            return result
        return None

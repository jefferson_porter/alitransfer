'''
Created on Jan 27, 2015

Module translating token information into a python 
aline token structure

@author: markus
'''

from commands import getstatusoutput as shellinteract
from datetime import datetime
from Datatransfer.PyAlien.AlienTools import alien_availability_test, AlienException
from Datatransfer.PyAlien.AlienToken import AlienToken

class AlienTokenReader(object):
    """
    Token info creator class
    """

    def __init__(self):
        '''
        Constructor
        '''
        self.__token = None
        
    def GetToken(self):
        return self.__token
        
    def CreateTokenInfo(self):
        """
        Create new token information
        """
        if not alien_availability_test():
            raise AlienException("AlienTokenCreator", "AlienNotFound", "")
        tokenstring = shellinteract("alien-token-info")[1]
        self.__token = self.__ParseKeys(tokenstring.split("\n"))
    
    def __ParseKeys(self, listofkeys):
        """
        Create the new alien token from a list of keys
        """
        result = AlienToken()
        for entry in listofkeys:
            mypair = entry.split(":")
            key = mypair[0].rstrip()
            value = None
            if len(mypair) == 2:
                value = mypair[1].lstrip()
            elif len(mypair) > 2:
                # In case of multiple occurrence of the delimiter, create new value as everything beside the leftmost delimiter
                value = entry[entry.find(":")+1:].rstrip().lstrip()
            self.__FillToken(result, key, value)
        return result
    
    def __FillToken(self, token, key, value):
        """
        Handle keys and fill token with information
        """
        if key == "Host":
            token.SetHost(value)
        elif key == "User":
            token.SetUser(value)
        elif key == "Port":
            token.SetPort(int(value))
        elif key == "Port2":
            token.SetPort2(value)
        elif key == "Pwd":
            token.SetPwd(value)
        elif key == "Nonce":
            token.SetNonce(value)
        elif key == "SID":
            token.SetSid(int(value))
        elif key == "Encr.Rep.":
            token.SetEncryption(int(value))
        elif key == "Expires":
            datehandler = AliDateHandler(value)
            token.SetExpiringDate(datetime(datehandler.GetYear(), datehandler.GetMonth(), datehandler.GetDay(), datehandler.GetHour(), datehandler.GetMinute(), datehandler.GetSecond()))
    
class AliDateHandler():
    """
    Helper class converting time information from alien tokens into
    python datetime
    """
    
    def __init__(self, datestring):
        """
        Constructor
        """
        self.__year = None
        self.__month = None
        self.__day = None
        self.__hour = None
        self.__minute = None
        self.__second = None
        self.__Parse(datestring)
        
    def __Parse(self, datestring):
        """
        Parse timestring into time format
        """
        tokens = self.__TokenizeSafe(datestring, " ")
        self.__GetMonth(tokens[1])
        self.__day = int(tokens[2])
        self.__ParseTime(tokens[3])
        self.__year = int(tokens[4])

    def __GetMonth(self, mystr):
        """
        Convert month string into month number
        """
        months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        for i in range(0,12):
            if mystr == months[i]:
                self.__month = i+1
                break
    
    def __ParseTime(self, timestr):
        """
        Convert time format into hours/minutes/seconds
        """
        tokens = timestr.split(":")
        self.__hour = int(tokens[0])
        self.__minute = int(tokens[1])
        self.__second = int(tokens[2])
        
    def __TokenizeSafe(self, inputstring, token):
        """
        Safe method to tokenize a string with delimiter, ignoring multiple occurrence of the
        same delimiter
        """
        result = []
        workingstring = inputstring
        running = True
        while running:
            nextfound = workingstring.find(token)
            if nextfound == -1:
                if len(workingstring):
                    result.append(workingstring)
                running = False
                break
            entry = workingstring[0:nextfound]
            if len(entry):
                result.append(entry)
            workingstring = workingstring[nextfound+1:]
        return result
    
    def GetYear(self):
        return self.__year
    
    def GetMonth(self):
        return self.__month
    
    def GetDay(self):
        return self.__day
    
    def GetHour(self):
        return self.__hour
    
    def GetMinute(self):
        return self.__minute
    
    def GetSecond(self):
        return self.__second
    
    def Print(self):
        print "Year: %s, Month: %s, Day: %s" %(self.__year, self.__month, self.__day)
        print "Hour: %s, Minute %s, Second: %s" %(self.__hour, self.__minute, self.__second)